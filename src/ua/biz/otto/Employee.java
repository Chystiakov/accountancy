package ua.biz.otto;


public class Employee {

    String name;
    String position;
    int salary;
    int salaryType;
    int monthSalary;
    int yearSalary;


    public Employee(String name, String position, int salary, int salaryType) {
        this.name = name;
        this.position = position;
        this.salary = salary;
        this.salaryType = salaryType;
        this.monthSalary = calculateMonthSalary();
        this.yearSalary = calculateYearSalary();
    }

    public int calculateMonthSalary() {
        switch (salaryType) {
            case 0:
                System.out.println("Name: " + name + " Position: " + position + " Month salary: " + salary);
                break;
            case 1:
                System.out.println("Name: " + name + " Position: " + position + " Month salary: " + salary * 8 * 20);
                break;
            case 2:
                System.out.println("Name: " + name + " Position: " + position + " Month salary: " + salary / 12);
                break;
        }
        return monthSalary;
    }

    public int calculateYearSalary() {
        switch (salaryType) {
            case 0:
                System.out.println("Year salary: " + salary * 12);
                break;
            case 1:
                System.out.println("Year salary: " + salary * 8 * 20 * 12);
                break;
            case 2:
                System.out.println("Year salary: " + salary);
                break;
        }
        return yearSalary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "Name " + name + '\'' +
                ", position = " + position +
                ", salary = " + salary +
                '}' + "\n";
    }


}
