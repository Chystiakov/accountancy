package ua.biz.otto;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        // write your code here
        ArrayList<Employee> staff = new ArrayList<>();


        staff.add(new Employee("Коренькова Анна Павловна", "менеджер по продажам", 115000, 2));
        staff.add(new Employee("Иванова Елена Львовна", "зам директора", 9500, 0));
        staff.add(new Employee("Петрова Татьяна Сергеевна", "менеджер по продажам", 115000, 2));
        staff.add(new Employee("Вакуленко Дмитрий Владимирович", "дизайнер", 7, 1));


        Collections.sort(staff, new SortByName());
        System.out.println(staff);


    }
}
